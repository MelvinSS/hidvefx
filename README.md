# README #

This is a sketch for making an HID controller with 5 analog axes and 6 buttons on Arduino Pro Micro (and Leonardo). HID lights will be implemented later on.

## Dependencies ##

For now, there are no dependencies required outside the ones already included in the folder.

## Features ##

1. 6 Buttons
2. 5 Axes

## Pinout ##

The potentiometer outputs needs to be connected to A0, A1, A2, A3, and A10 which should be labeled as pin 10, 18, 19, 20, and 21 on the Arduino Pro Micro.

One of the terminal for each buttons should be connected on pin 4, 5, 6, 7, 8, and 9. The other terminal is connected to ground.

## ToDo ##

Implement WS2812B for HID Lighting.