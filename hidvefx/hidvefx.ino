// ====================================================
// HID VEFX Board
//
// Sliders are connected to pins 18, 19, 20, 21, and 10
// Buttons are on pin 4, 5, 6, 7, 8, and 9
// ====================================================

#include "vefxboard.h"
#define BUTTON_COUNT 6
vefxReport_t report;

int buttonPins[BUTTON_COUNT] = {4, 5, 6, 7, 8, 9};

void setup() {
  for (int i = 0 ; i < BUTTON_COUNT; i++) {
    pinMode(buttonPins[i], INPUT_PULLUP);
  }
}

void loop() {
  readAnalogSliders();
  readButtons();
  vefxb.setState(&report);
  delayMicroseconds(2000);
}

