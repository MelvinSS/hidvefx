/*
  Keyboard.cpp

  Copyright (c) 2015, Arduino LLC
  Original code (pre-library): Copyright (c) 2011, Peter Barrett

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "vefxboard.h"

#if defined(_USING_HID)

//================================================================================
//================================================================================
//	vefxb descriptor

static const uint8_t _hidReportDescriptor[] PROGMEM = {

  0x05, 0x01,                    /* USAGE_PAGE (Generic Desktop) */
  0x09, 0x04,                    /* USAGE (Game Pad) */
  0xa1, 0x01,                    /* COLLECTION (Application) */
  0x85, 0x04,				 	   /*   REPORT_ID */ //REPORT 4
  /*Buttons */
  0x05, 0x09,                    /*     USAGE_PAGE (Button) */
  0x19, 0x01,                    /*     USAGE_MINIMUM (Button 1) */
  0x29, 0x08,                    /*     USAGE_MAXIMUM (Button 8) */
  0x15, 0x00,                    /*     LOGICAL_MINIMUM (0) */
  0x25, 0x01,                    /*     LOGICAL_MAXIMUM (1) */
  0x95, 0x06,                    /*     REPORT_COUNT (6)*/
  0x75, 0x01,                    /*     REPORT_SIZE (1) */
  0x81, 0x02,                    /*     INPUT (Data,Var,Abs) */
  /* Reserved byte */
  0x95, 0x01,                      /*   REPORT_COUNT (1) */
  0x75, 0x02,                      /*   REPORT_SIZE (2) */
  0x81, 0x03,                      /*   INPUT (Cnst,Var,Abs) */
  /*Axis */
  0x05, 0x01,                    /*     USAGE_PAGE (Generic Desktop) */
  0x09, 0x33,                    /*     USAGE (Rx) */
  0x09, 0x34,                    /*     USAGE (Ry) */
  0x09, 0x35,                    /*     USAGE (Rz) */
  0x09, 0x36,                    /*     USAGE (Slider) */
  0x09, 0x37,                    /*     USAGE (Dial) */
  0x15, 0x00,                    /*     LOGICAL_MINIMUM (0) */
  0x26, 0xff, 0x00,              /*     LOGICAL_MAXIMUM (255) UNSIGNED */
  0x95, 0x05,                    /*     REPORT_COUNT (5) */
  0x75, 0x08,                    /*     REPORT_SIZE (8) */
  0x81, 0x02,                    /*     INPUT (Data,Var,Abs) */

  //iivx led

  /*Lights config*/
  0x85, 0x05,				 /*   REPORT_ID set to 5*/
  0x15, 0x00,                    /*     LOGICAL_MINIMUM (0) */
  0x25, 0x05,                    /*     LOGICAL_MAXIMUM (5) */
  /*Led 1 */
  0x05, 0x0a,                    /*     USAGE_PAGE (Ordinals) */
  0x09, 0x01,                    /*     USAGE (Instance 1) */
  0xa1, 0x02,                    /*     COLLECTION (Logical) */
  0x05, 0x08,                    /*       USAGE_PAGE (LEDs) */
  0x09, 0x4b,                    /*       USAGE (Generic Indicator 1) */
  0x75, 0x01,                    /*       REPORT_SIZE (1) */
  0x95, 0x01,                    /*       REPORT_COUNT (1) */
  0x91, 0x02,                    /*       OUTPUT (Data,Var,Abs) */
  0xc0,                          /*     END_COLLECTION */
  /*Led 2 */
  0x05, 0x0a,                    /*     USAGE_PAGE (Ordinals) */
  0x09, 0x02,                    /*     USAGE (Instance 2) */
  0xa1, 0x02,                    /*     COLLECTION (Logical) */
  0x05, 0x08,                    /*       USAGE_PAGE (LEDs) */
  0x09, 0x4b,                    /*       USAGE (Generic Indicator 1) */
  0x75, 0x01,                    /*       REPORT_SIZE (1) */
  0x95, 0x01,                    /*       REPORT_COUNT (1) */
  0x91, 0x02,                    /*       OUTPUT (Data,Var,Abs) */
  0xc0,
  /*Led 3 */
  0x05, 0x0a,                    /*     USAGE_PAGE (Ordinals) */
  0x09, 0x03,                    /*     USAGE (Instance 3) */
  0xa1, 0x02,                    /*     COLLECTION (Logical) */
  0x05, 0x08,                    /*       USAGE_PAGE (LEDs) */
  0x09, 0x4b,                    /*       USAGE (Generic Indicator 1) */
  0x75, 0x01,                    /*       REPORT_SIZE (1) */
  0x95, 0x01,                    /*       REPORT_COUNT (1) */
  0x91, 0x02,                    /*       OUTPUT (Data,Var,Abs) */
  0xc0,
  /*Led 4 */
  0x05, 0x0a,                    /*     USAGE_PAGE (Ordinals) */
  0x09, 0x04,                    /*     USAGE (Instance 4) */
  0xa1, 0x02,                    /*     COLLECTION (Logical) */
  0x05, 0x08,                    /*       USAGE_PAGE (LEDs) */
  0x09, 0x4b,                    /*       USAGE (Generic Indicator 1) */
  0x75, 0x01,                    /*       REPORT_SIZE (1) */
  0x95, 0x01,                    /*       REPORT_COUNT (1) */
  0x91, 0x02,                    /*       OUTPUT (Data,Var,Abs) */
  0xc0,
  /*Led 5 */
  0x05, 0x0a,                    /*     USAGE_PAGE (Ordinals) */
  0x09, 0x05,                    /*     USAGE (Instance 5) */
  0xa1, 0x02,                    /*     COLLECTION (Logical) */
  0x05, 0x08,                    /*       USAGE_PAGE (LEDs) */
  0x09, 0x4b,                    /*       USAGE (Generic Indicator 1) */
  0x75, 0x01,                    /*       REPORT_SIZE (1) */
  0x95, 0x01,                    /*       REPORT_COUNT (1) */
  0x91, 0x02,                    /*       OUTPUT (Data,Var,Abs) */
  0xc0,
  /*Led 6 */
  0x05, 0x0a,                    /*     USAGE_PAGE (Ordinals) */
  0x09, 0x06,                    /*     USAGE (Instance 6) */
  0xa1, 0x02,                    /*     COLLECTION (Logical) */
  0x05, 0x08,                    /*       USAGE_PAGE (LEDs) */
  0x09, 0x4b,                    /*       USAGE (Generic Indicator 1) */
  0x75, 0x01,                    /*       REPORT_SIZE (1) */
  0x95, 0x01,                    /*       REPORT_COUNT (1) */
  0x91, 0x02,                    /*       OUTPUT (Data,Var,Abs) */
  0xc0,
  /*  Reserved 10 bits */
  0x95, 0x01,						 /*   REPORT_COUNT (1) */
  0x75, 0x0a,						 /*   REPORT_SIZE (10) */
  0x91, 0x03,						 /*   OUTPUT (Cnst,Var,Abs) */
  /*Footer */
  0xc0                          /* END_COLLECTION */
};

vefxb_::vefxb_(void)
{
  static HIDSubDescriptor node(_hidReportDescriptor, sizeof(_hidReportDescriptor));
  HID().AppendDescriptor(&node);
}

#define vefxByte 6 		// should be equivalent to sizeof(JoyState_t)

void vefxb_::setState(vefxReport_t *report)
{
  uint8_t data[vefxByte];
  uint8_t buttonTmp;
  buttonTmp = report->buttons;

  data[0] = buttonTmp;		// Break 16 bit button-state out into 2 bytes, to send over USB

  data[1] = report->slider1;		// X axis
  data[2] = report->slider2;		// Y axis
  data[3] = report->slider3;		// Z axis
  data[4] = report->slider4;    // Slider
  data[5] = report->slider5;    // Dial

  //HID_SendReport(Report number, array of values in same order as HID descriptor, length)
  HID().SendReport(4, data, vefxByte);
}

vefxb_ vefxb;

#endif
