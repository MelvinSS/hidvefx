void readAnalogSliders() {
  report.slider1 = (uint16_t)remapSliders(analogRead(A0));
  report.slider2 = (uint16_t)remapSliders(analogRead(A1));
  report.slider3 = (uint16_t)remapSliders(analogRead(A2));
  report.slider4 = (uint16_t)remapSliders(analogRead(A3));
  report.slider5 = (uint16_t)remapSliders(analogRead(A10));
}

uint8_t remapSliders(int value) {
  uint8_t returnVal = map(value, 0, 1023, 0, 255);
  return returnVal;
}

void readButtons() {
  for (int i = 0; i < BUTTON_COUNT ; i++) {
    if (digitalRead(buttonPins[i]) != HIGH) {
      report.buttons |= (uint16_t)1 << i;
    } else {
      report.buttons &= ~((uint16_t)1 << i);
    }
  }
}

